package com.cliqueusuarionew.clique.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cliqueusuarionew.clique.model.UsuarioModel;
import com.cliqueusuarionew.clique.repository.UsuarioRepository;

@Controller
public class UsuarioController {
	@Autowired UsuarioRepository usuarioRepository ;
	
	@RequestMapping(path="/Usuario", method=RequestMethod.POST)
	@ResponseBody
	public UsuarioModel inserirUsuario(@RequestBody UsuarioModel usuario) {
		return usuarioRepository.save(usuario);
	}
}
