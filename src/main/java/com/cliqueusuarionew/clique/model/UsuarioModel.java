package com.cliqueusuarionew.clique.model;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UsuarioModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String usuario;
	private String navegador;
	private Date timestamp;

	public void setId(long id) {
		this.id = id;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
