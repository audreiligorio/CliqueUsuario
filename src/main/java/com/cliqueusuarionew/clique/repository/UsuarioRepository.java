package com.cliqueusuarionew.clique.repository;


import org.springframework.data.repository.CrudRepository;

import com.cliqueusuarionew.clique.model.UsuarioModel;

public interface UsuarioRepository extends CrudRepository<UsuarioModel, Integer>{

}
